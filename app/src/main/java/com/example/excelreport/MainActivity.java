package com.example.excelreport;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import static jxl.Workbook.createWorkbook;

public class MainActivity extends AppCompatActivity {

    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //bottun reference
        b1 =  (Button)findViewById(R.id.button);

    }



    public void writexcel(View v)
    {
        final String fileName = "TodoList.xls";

        //Saving file in external storage
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard.getAbsolutePath() + "/javatechig.todo");

        // Toast 1
        Toast.makeText(getApplication(),
                "File...", Toast.LENGTH_SHORT).show();

        //create directory if not exist
        if(!directory.isDirectory()){
            directory.mkdirs();
        }
        // Toast 2
        Toast.makeText(getApplication(),
                "File...2", Toast.LENGTH_SHORT).show();

        //file path
        File file = new File(directory, fileName);

        // workbook settings
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));

        //workbook object
        WritableWorkbook workbook;

        //Toast 3
        Toast.makeText(getApplication(),
                "File...3", Toast.LENGTH_SHORT).show();

        try {
            // Toast 4
            Toast.makeText(getApplication(),
                    ""+file, Toast.LENGTH_SHORT).show();

            // Create workbook
            workbook = createWorkbook(file);

           // Toast 5
            Toast.makeText(getApplication(),
                    ""+workbook, Toast.LENGTH_SHORT).show();

            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("MyShoppingList", 0);

            //Toast 6
            Toast.makeText(getApplication(),
                    "shopping...", Toast.LENGTH_SHORT).show();

            try {
                //Adding Headdings for columns
                sheet.addCell(new Label(0, 0, "Subject")); // column and row
                sheet.addCell(new Label(1, 0, "Description"));

                for(int i=1;i<20;i++) {
                    //Adding Repetitive row
                    sheet.addCell(new Label(0, i, "manish"));
                    sheet.addCell(new Label(1, i, "kuttta"));
                }
                //TOast 7
                Toast.makeText(getApplication(),
                        "celladded...", Toast.LENGTH_SHORT).show();

            } catch (RowsExceededException e) {
                e.printStackTrace();

            } catch (WriteException e) {
                e.printStackTrace();
            }
            // Writing workbook
            workbook.write();
            try {
                //Toast 8
                Toast.makeText(getApplication(),
                        "Writing...", Toast.LENGTH_SHORT).show();

                //Workbook close
                workbook.close();

                //Toast 9
                Toast.makeText(getApplication(),
                        "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();

            } catch (WriteException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
